
O presente capítulo apresenta conceitos básicos para a compreensão do conteúdo proposto. Neste, busca-se contextualizar o projeto, através da revisão da arte e seus conceitos como entendidos atualmente.

\section{CONCEITOS BÁSICOS}

Esta seção desenvolverá conceitos fundamentais para a compreensão da problemática resolvida com o sistema proposto. O objeto de estudo emerge à evidência quando observados dois atores opostos: a complexidade do sistema e sua robustez. Estes são primeiramente compreendidos pela natureza de um sistema embarcado.

\subsection{SISTEMA EMBARCADO}

Segundo \citetexto{Ganssle2003EmbeddedDictionary}, um sistema embarcado é uma combinação entre hardware (HW) e software (SW) dedicada à função específica - podendo fazer parte de um sistema maior. Este sistema aplicado para o controle de funcionalidades mecânicas, como válvulas hidráulicas, em conjunto com outras funcionalidades em tempo real gera um grau de complexidade elevado. \textit{Electronic Control Unit} (ECU) ou Unidades de Controle Eletrônico são exemplos de subsistemas embarcados com acionamentos elétricos, hidráulicos e mecânicos e estão presentes em todo maquinário agrícola autopropelido.

\subsection{MAQUINÁRIO AGRÍCOLA AUTOPROPELIDO}

Entende-se como uma máquina agrícola autopropelida (ou simplesmente máquina) qualquer máquina com a finalidade de beneficiar uma cultura, possuindo uma fonte de locomoção incorporada. Em particular, são máquinas agrícolas aplicáveis neste contexto: tratores, colheitadeiras, colhedoras de cana e pulverizadores. A quantidade de acionamentos e a classe do motor determinam o número de ECUs que a máquina comporta \cite{paquet1996method}.

Exigência do mercado e de regulamentações criam perspectivas de aumento da inteligência em veículos automotivos, o aumento de funcionalidade e de complexidade do sistema incrementam o risco de falhas \cite{Zhang2010Model-basedApplication}, \cite{Nayak2010AutomatedHardware-in-the-loop}. Esta tendência se confirma em máquinas agrícolas, que enfrenta desafios similares aos veículos automotores. Porém, estes desafios são atenuados pela realidade do local de operação do maquinário - inóspito para sistemas sensíveis.

Falhas são apresentadas como custos de materiais, de energia e de oportunidades. Busca-se então, reduzir o máximo possível falhas sistêmicas de projeto apresentadas antes do lançamento do produto no mercado. Tem-se como ferramenta o perfil e identificação destas falhas, segundo sua fonte e natureza. Estas propriedades são interpretadas pela Curva da Banheira.

\subsection{CURVA DA BANHEIRA}

A evolução de um produto consiste em três etapas: desenvolvimento, lançamento e \textit{phase-out}. Cada fase é caracterizada por tipos de ocorrências de falhas que podem ser estudadas pela Curva da Banheira (\fig{CDB}). Este gráfico traça a densidade de probabilidade de falhas do produto em relação à sua vida útil.

\begin{figure}[ht]
	\caption{Curva da Banheira} 
	\label{fig:CDB}
	\centering
	\begin{minipage}{1\textwidth}
		\includegraphics[width=\textwidth]{CDB.PNG}
		\fonte{Adaptado de \cite{ross2016functional}}%pg 47
	\end{minipage}
\end{figure}

Fica importante anotar, que por produto se entende qualquer HW ou SW que é ou possui a intensão de ser comercializado.

A figura acima apresenta a curva típica de taxa de falha de produtos de engenharia no mercado. A taxa de falha observada pode ser decomposta em três taxas de falhas, segregando-as por natureza. Uma destas taxas constante (em verde) e irremediável, já que é intrínseca de qualquer projeto. A taxa acendente no tempo (em amarelo), representa os efeitos de envelhecimentos sofridos pelo uso do produto. Por último, de interesse deste trabalho, a taxa de falha decrescente (em vermelho) retrata falhas de projeto que não foram eliminadas ao lançar o produto. No entanto, são estas previsíveis e contornáveis.

Enquanto que esta curva é muito utilizada em previsões prematuras \cite{ross2016functional}, esta vem sendo questionada quanto aplicabilidade e veracidade das informações que ela contém \cite{Klutke2003ACurve}. No entanto, nela emerge facilmente a questão da qualificação da falha quanto sua natureza. Falhas podem ser separadas como erros sistemáticos ou erros aleatórios, enquanto que falhas que ocorrem ao longo do tempo de vida do produto são aleatórias, imprevisíveis e constantes (Figura \ref{fig:CDB}). Na fase de desenvolvimento, erros são em maior parte sistemáticos e contornáveis. Pois possuem como origem falhas técnicas de projeto.

Com o objetivo de remover todas as falhas sistemáticas, algumas técnicas são aplicadas. São estas \textit{Software-In-the-Loop} (SwIL), \textit{Model-In-the-Loop} (MIL) durante os primeiros passos do projeto, para respectivamente a validação de SW e Validação e Verificação (V\&V) da planta de controle. Em seguida, possui-se o \textit{Process-In-the-Loop} (PIL) o qual V\&V o SW no HW final. E o \textit{Hardware-In-the-Loop} (HIL) com o \textit{System-In-the-Loop} (SIL) para a V\&V respectivamente de um HW e SW finais e o conjunto de HW e SW finais (sistema). Consequentemente, o HIL é introduzido como plataforma de validação de projeto - sendo assim, sinônimos.

\subsection{HARDWARE-IN-THE-LOOP}

\textit{Hardware-In-the-Loop} (HIL) é definido como qualquer ambiente de simulação o qual parte do HW testado está interfaceado com outros componentes simulados \cite{Ganssle2003EmbeddedDictionary}. A simulação pode tanto ocorrer com laços de controle abertos, fechados ou uma combinação de ambos (\fig{HILgenerico}).

\begin{figure}[ht]
	\caption{Representação do conceito do HIL genérico}
    \label{fig:HILgenerico}
	\centering
	\begin{minipage}{.63\textwidth}
		\includegraphics[width=\textwidth]{ConceitoHIL.png}
		\fonte{Adaptado de \cite{artigoHIL}.}
	\end{minipage}
\end{figure}

Técnica de projeto consagrada, foi primeiro desenvolvida nas indústrias aeronáuticas e de defesa, data dos anos de 1930 \cite{Rankin2011ASystems}. Com o desenvolvimento da indústria, principalmente automotiva, o HIL passou a ser um requisito no desenvolvimento do produto.

\begin{figure}[ht]
	\caption{Modelo V de desenvolvimento}
    \label{fig:ModeloV}
	\centering
	\begin{minipage}{.82\textwidth}
		\includegraphics[width=\textwidth]{ModeloV.PNG}
		\fonte{Adaptado de \cite{Kum2006AutomatedSystems}.}
	\end{minipage}
\end{figure}

Conforme o Modelo V (\fig{ModeloV}), o HIL é a penúltima etapa de validação do produto (Teste de Integração do Sistema). Pois esta etapa verifica o conjunto de HWs e SWs em última versão integrados. Consequentemente obtendo os resultados mais fiéis às condições reais. Com o benefício de amplificação dos Casos de Teste (CT), é possível emular situações irreprodutíveis em campo, verificando a segurança do sistema \cite{artigoHIL}. 

Falhas e deficiências de projeto (erros sistêmicos) podem em grande parte ser removidas do produto final com esta metodologia. Isto traz benefícios financeiros e não mensuráveis para o fabricante produto. Pois, uma vez que o produto possui qualidade, ele é reconhecido no mercado como superior - incorporando valor do ponto de vista do cliente. Ademais, o custo inicial de construção de um HIL tende a cobrir o custo de uma validação em campo. Isto é particularmente verdadeiro quando o produto se trata de uma colheitadeira. De valor elevado e inconveniente de transporte, uma colheitadeira para ser testada em campo deve colher um plantio cedido por um empreendedor rural \cite{TCCbruno}. A potencialidade desta plataforma pode ser expandida com a automatização deste processo.

\subsection{AUTOMATIZAÇÃO}

Em uma primeira geração, o HIL proposto é em grande parte manual \cite{TCCbruno}. Esta metodologia possui uma vantagem de validação e prova de conceito - a um custo de Homem-hora (HH) elevado. Por motivos de organização e nomenclatura, o autor nomeia o HIL ``manual'' como ``HIL 1.0''; o HIL ``automatizado'' de ``HIL 2.0''. O HIL 1.0: com um operador acionando entradas de controle de teste e observando os resultados é válido para a verificação funcional completa do maquinário. Entretanto, o sistema também apresenta algumas oportunidades de melhoramento. São elas:

\begin{itemize}
\item Erro humano é capaz de inserir falhas na execução do processo de V\&V, o que pode parcialmente comprometer os resultados;
\item O custo do HH envolvido e limitação temporais obrigam o teste:
\begin{itemize}
\item a ter tempo reduzido;
\item proceder de forma uniforme e linear quanto os itens a serem V\&V;
\end{itemize}
\item CT são reduzidos; e
\item o ambiente simulado (condições) não é monitorado em sua totalidade.
\end{itemize}

Todas estas inadequações são solucionadas com a automatização. Além mais, a automatização da plataforma HIL possui como vantagens \cite{Kum2006AutomatedSystems}:

\begin{itemize}
\item redução do HH para a execução do ensaio;
\item expansão do tempo de uso da plataforma;
\item expansão dos CT;
\item variação na ordem de execução dos CT.
\end{itemize}

Sendo o último item citado particularmente importante, pois, em um sistema real, a ordem das operações podem causar falhas de sistema. Como o tempo de uso da plataforma é expandido os CT também, o rigor da plataforma a uma situação real em si é superiora \cite{Ploger2004AutomatisierterElektroniksysteme}, \cite{Nayak2010AutomatedHardware-in-the-loop}, \cite{Kum2006AutomatedSystems}. Isto representa um avanço da perspectiva que o HIL é responsável pela eliminação de falhas sistêmicas no momento de desenvolvimento do projeto. Como isto já é alcançado com o HIL 1.0, o HIL 2.0 possibilita a redução de falhas randômicas. Porque a plataforma executa os CT em sequências diferentes, as falhas que poderiam emergir por conta de uma ordem em particular são eliminadas.

Conforme já comentado, a natureza do produto desenvolvido abre espaço para eventos infortunos. Por este motivo, garantir a segurança do maquinário quanto ao seu bom funcionamento e à saúde do operador possui grande importância. Todos os elementos V\&V devem observar as melhores práticas da indústria, conforme às técnicas de Segurança Funcional (SF).

\subsection{SEGURANÇA FUNCIONAL}

Todos os produtos que apresentam um grau de risco (\textit{risk}) ou perigo (\textit{hazard}) devem conter métodos de mitigação ou eliminação em seus projetos (\textit{safety related}). Esta lei é aplicável para todas as áreas da engenharia. Adicionalmente, a eletrônica, vanguarda da arte, propõe normatização importantes para técnicas aplicadas nas áreas de automatização industrial, automotiva e equipamentos agrícolas e florestais. Dentro destas normas, a \citetexto{ISO25119} e também \citetexto{ISO26262}, definem SF como a "ausência de risco irrazoável devido a perigo causado pelo mau funcionamento do sistema eletroeletrônico." \cite{ISO26262}.

O conteúdo destas normas propõe ferramentas conceituadas como \textit{Failure Mode and Effects Analysis} (FMEA) que contém \textit{Fault Tree Analysis} (FTA), \textit{Event Tree Analysis} (ETA), que para \textit{Hazard Assesment and Risk Analysis} (HARA). Estes conceitos são porém, mais antigos que as próprias normas e procuram evidenciar boas práticas de gerenciamento de projeto a fim de garantir um produto tecnicamente seguro \cite{StandardsFunctionalStandards}. Um número grande de ferramentas são de conhecimento do estudo de análise de riscos. Porém, em uso geral, as ferramentas FTA e ETA são as mais conhecidas e terão foco nos próximos parágrafos.

Em uma primeira etapa, o conceito do sistema, dividido em subsistemas. A cada subsistema, uma análise detalhada da severidade de um modo de falha e sua respectiva probabilidade é contabilizada. O risco resultante é então classificado dentre um \textit{Agricultural Performance Level} (AgPL), que servirá como requisito de aprovação do subsistema. Em seguida, a FTA é capaz de analisar indutivamente as causas dos riscos, indicando o que os ocasionam. Um exemplo de uso desta ferramenta é exposto na \fig{FTAex} a seguir.

\begin{figure}[ht]
	\caption{Exemplo FTA simples}
    \label{fig:FTAex}
	\centering
	\begin{minipage}{.6\textwidth}
		\includegraphics[width=\textwidth]{FTA.PNG}
		\fonte{Adaptado de \cite{NASA2002FaultApplications}.}
	\end{minipage}
\end{figure}

Para este exemplo, as probabilidade de falha $PF$ dos subsistemas \textsc{G1} e \textsc{G2} é obtida através da probabilidade de falha de seus respectivos subsistemas:

\begin{gather}
PF(\text{G2}) = PF(\text{B}) + PF(\text{C}), \\
PF(\text{G1}) = PF(\text{A}) \cdot PF(\text{G2}).
\end{gather}

\newpage

Inversamente ao FTA, o ETA possui uma abordagem dedutiva. Esta ferramenta, analisa cronologicamente, como uma falha de componente impacta no subsistema. Um exemplo de aplicação é exposto em \fig{ETAex}.

\begin{figure}[ht]
	\caption{Exemplo ETA de um sistema de pressurização simples}
    \label{fig:ETAex}
	\centering
	\begin{minipage}{.8\textwidth}
		\includegraphics[width=\textwidth]{ETA.png}
		\fonte{Adaptado de \cite{modarres1999reliability}.}
	\end{minipage}
\end{figure}

Desta maneira, é possível deduzir de forma similar a probabilidade de falha resultante, com base da confiabilidade $R$ do evento, tal que $R(\text{evento}) = 1 - PF(\text{evento})$.

\begin{gather}
\begin{aligned}
PF(\text{resultado}) = PF(A) + PF(A)\cdot R(B) &+ PF(A)\cdot PF(B)\cdot R(C) + \\
&+ PF(A)\cdot PF(B)\cdot PF(C)
\end{aligned}
\end{gather}

Esta análise avançada do subsistema fornece ao projetista do HW e/ou SW, detalhes sobre a fragilidades de segurança inerente do projeto. Aliado deste conhecimento, técnicas podem ser aplicadas para a mitigação ou eliminação dos riscos.

Com o HW e SW planejados, dá-se início à análise da probabilidade de falhas do subsistema conforme a descrição do FTA/ETA. Para o cálculo de falhas, cada componente deve possuir uma base de dados real. Como exemplo, ocorrências de falha em campo de produtos, ocorrências de peças de garantia. Caso uma base de dados como tal não estiver disponível por implementação recente da SF no modelo de desenvolvimento da empresa, a experiência de engenharia é fator determinante de um valor numérico \cite{ISO26262}. Caso a meta AgPL não for atingida, um número de interações são tomadas até o alcance do objetivo de segurança especificado.

Uma vez que interações suficientes são realizadas, segundo o Modelo V, o projeto deve ser testado em plataformas de teste adequadas como a MIL, SwIL e HIL. Somente com a aprovação do Engenheiro de Testes e a documentação FMEA completa, deve o primeiro protótipo ser montado para a validação em campo. O projeto V\&V como protótipo, é apto para a produção com a máxima segurança, como é requisito para o subsistema do motor eletrônico.

\subsection{MOTOR ELETRÔNICO}

Por definição, veículos autopropelidos são dotados de uma fonte de locomoção. Tipicamente, em uma máquina agrícola autopropelida, motores de combustão interna a diesel são empregados. Como qualquer outro tipo de motor, estes possuem variáveis a serem controladas em operação. Em carros, por exemplo, a velocidade é a variável regulada. Simplificadamente, o controle de velocidade é feito gerenciando a entrada da mistura combustível/oxigênio. Historicamente, este tipo de controle foi feito por um regulador (borboleta, do inglês \textit{governor}) mecânico \cite{block1954load}.

Atualmente, restrições legais são impostas para estipular o nível máximo de poluição gerada por um motor de combustão de uma classe (potência) específica na Europa e nos Estados Unidos. No Brasil, recentemente o Conselho Nacional do Meio Ambiente do Ministério do Meio Ambiente impõe restrição de poluição do sonora e do ar para máquinas agrícolas e rodoviárias pela resolução \citetexto{conama}, MAR-I (Máquinas
Agrícolas e Rodoviárias - Fase 1). Abrevia-se como Motor Tier III, aquele que se adeque à norma americana de emissões de poluentes (\textit{Tier III}), equivalente àquela em vigor no Brasil. Segundo \citetexto{guiaMAR1}, algumas tecnologias vão de encontro à redução de emissões de partículas e de óxidos nitrogenados, que, ao mesmo tempo, melhoram o desempenho do motor. Entre elas, toma-se interesse no Controle Eletrônico de Injeção.

O motor controlado eletronicamente, comumente chamado de Motor Eletrônico, possui outras vantagens, como \cite{Mendonca2017DevelopmentTasks}:

\begin{itemize}
\item Maior diagnóstico de variáveis do motor, como velocidade de giro, pressão do pistão, temperatura do motor, fluxo de óleo, entre outros;
\item Diagnóstico digital em tempo real do veículo;
\item Configurabilidade de parâmetros ou perfis (SW).
\end{itemize}

Para o tanto, uma ECU especializada é empregada exclusivamente para o controle do motor.

Tendo observado todos os conceitos básicos relevantes ao projeto, analisa-se o estado tecnológico atual. Um resumo de trabalhos relevantes será apresentados no Estado da Arte.

\section{ESTADO DA ARTE}

Conforme discutido na seção de Referencial Bibliográfico, SF e simulações \textit{X-In-the-Loop} estão intrinsecamente ligados. Em particular, a plataforma HIL obtém espaço incontornável na indústria \textit{safety related}, pois o HIL faz parte obrigatória do desenvolvimento de um projeto, conforme as ISO25119 (SF agrícola) e ISO26262 (SF automotiva); e o Modelo V. Os seguintes trabalhos selecionados apresentam o que há de ponta na indústria atual sobre os temas de automatização de ensaios, HIL e SF. Dá-se ênfase nos resultados obtidos graça à aplicação destas técnicas.

Para fins de evitar desentendimento, faz-se necessário definir alguns termos técnicos que são referenciados no texto:

\begin{itemize}
\item Produto: Produto final do projeto sendo desenvolvido. Exemplo: Trator;
\item Plataforma: Sistema ou conceito cuja finalidade é servir de ferramenta para estudo de outro sistema. Exemplo: HIL;
\item Sistema: Conjunto de subsistemas que, em coesão, formam um mecanismo autogerenciado, capaz de realizar funções definidas. Exemplo: Carro;
\item Subsistema: Qualquer parte de um sistema ou subsistema que possua uma finalidade específica, sinal de entrada e sinal ou função de saída e seja composto por mais que um componente. Exemplo: freio eletrônico (\textit{break-by-wire});
\item Componente: Parte de um sistema ou subsistema que possua sinal de entrada e sinal de saída - sinônimo de \textit{Device}. Exemplo: ECU;
\item Módulo: Qualquer subsistema fisicamente removível do sistema, que venha a compô-lo em quantidade dependente da configuração do sistema. Exemplo: Cartão de saída analógica (AO) de um chassis de instrumentação.
\end{itemize}

Sistemas automotivos controlados eletronicamente, conhecidos como \textit{X-by-Wire} estão se tornando cada vez mais comuns, substituindo funções críticas (\textit{safety critical}) do veículo a cada passo. A complexidade do sistema faz com que a norma ISO26262 seja de vital importância para o projeto do sistema a prova de falhas. \citetexto{Leu2015AnStandard} apresentam um sistema \textit{Brake-by-Wire} inteligente para carros. Os autores propõem que o sistema de freio inteligente possa ser comandado por uma ECU, com base em dados provenientes de sensores de distância frontal, velocímetro e pedal do operador. A informação obtida pelo radar é considerada para o acionamento do freio em modo de emergência com o propósito de evitar uma colisão frontal. O sistema é analisado conforme as técnicas HARA, ETA, FTE e FMEA conforme a norma vigente a fim de identificar fraquezas do sistema. Usando dados gerados pelos autores, eles demonstram como a metodologia de SF categoriza o sistema em um nível de segurança mandatório. Uma sugestão de modificação no sistema é dada como exemplo, para que o sistema atinja a meta especificada.

Verificação funcional é um ponto importante na verificação global do projeto. \citetexto{Raut2016TestTesting} consideram os resultados de um HIL implementado sobre a ECU de motor, para a verificação do algoritmo \textit{Engine Warmup}. Este algoritmo é responsável por limitar a velocidade e o torque do motor ao parti-lo, até que as pressões de óleo e líquido arrefecedor pré-definidas serem atingidas. Caso este procedimento falhar, gasto e envelhecimento prematuro do motor podem ocorrer. Os resultados positivos apresentados pelos autores reafirmam que plataforma HIL é adequada para um testes estáticos, reprodutíveis e fiéis.

A indústria de maquinário agrícola, também investe em plataformas HIL para a V\&V do produto. \citetexto{artigoHIL} propõem uma plataforma HIL de uma colheitadera híbrida. Contrariamente a testes de carro, uma colheitadeira é maior e possui funções específicas de sua operação, além das funções do motor. Isso resulta em um sistema mais complexo, com mais sensores. Os autores expressam que o sistema é capaz de eliminar todos os erros sistemáticos de SW, capacitando a máquina para o teste no campo. Com uma janela curta de teste de campo, é importante que a máquina não seja impedida de colher por um erro previsível em laboratório. Não obstante, a ausência de erros no produto é bem vista pelo cliente. O texto também afirma que o sistema desenvolvido possui vantagens como repetibilidade, cobertura de testes garantida, viabilidade econômica, tempo de teste reduzido e mitigação dos riscos.

A implementação de CT de uma linguagem natural para uma linguagem de programação é um passo fundamental para a automatização de testes. Este tema é estudado por \citetexto{KabsuHan2013ATesting} para uma rede de veículo inteligente, que contém ECUs e sensores interligados. Os autores buscam implementar os testes em uma metologia baseada em modelo. E encontram em seu estudo de caso motivos para o uso de Máquina de Estados Finitos (MEF) para concretizar seus testes. A melhor metodologia de cobertura de teste é ponderada, com o dilema entre cobertura/confiabilidade e Tempo de Teste (TT) no foco, pois, a taxa de falhas obtidas por segundo é muito maior no início do teste que ao final. O valor intelectual obtido ao deixar o teste rodando por um tempo oneroso não compensa o custo em TT. O texto confirma que a automatização aumenta a cobertura de testes e minimiza CT, ainda aumentando a rastreabilidade de teste.

A tendência da indústria é procurar simulações mais fiéis, automatizadas e com relatório de resultados automático. Pesquisadores da Universidade de Pannonia (Hungria) e do laboratório da Continental Automotiva Hungria  \citetexto{Enisz2014ReconfigurableVerification} desenvolveram em cooperação uma plataforma HIL adaptável, reconfigurável, para a simulação de veículo de estrada. A parte automotiva testada nesta plataforma (ECU, Unidade de Controle Hidráulico, ECU ABS) interage com uma simulação virtual de veículo dinâmico (veDyna), que determina sinais de sensores simulados a serem realimentados ao \textit{Device Under Test} (DUT). A simulação do veículo dinâmico veDyna possui um perfil de estrada que o carro deverá seguir. Este perfil contém relevo do terreno, obstáculos e outros carros simulados interagindo no mesmo ambiente. O sistema também possui uma unidade de inserção de falhas eletrônicas para o teste de confiabilidade. O teste pode ser reconfigurável para se adaptar aos CT do DUT. O gerenciador do teste também é capaz de gerar relatório de resultados automático. CT para a V\&V de uma ECU de freio comercial foram executados e os resultados de acordo com o esperado. A plataforma demonstrou-se ser flexível e de fácil aprendizado.



\section{PESQUISA DE MERCADO}

Por conta do HIL ser uma plataforma consagrada no mercado, algumas soluções são comercializadas. Estas soluções são produtos que visam, de modo geral, oferecer ferramentas padrões usadas neste tipo de simulação, que intrinsecamente desconsideram a especificidade de cada máquina simulada. Esta seção apresenta e discute algumas destas utilidades. Por fim, uma análise comparativa de cada solução em relação ao projeto proposto é feita.

Conforme visto previamente, o HIL é composto por duas partes: o HW com SW de máquina e o HW com SW para simulação, sendo este último uma subsistema que possui como finalidade a simulação do ambiente e de reação do ambiente em relação à máquina, implementado com a substituição de sensores pelo interfaceamento com este subsistema (\fig{ConHIL}).

\begin{figure}[ht]
	\caption{Conceito simulação HIL}
    \label{fig:ConHIL}
	\centering
	\begin{minipage}{1\textwidth}
		\includegraphics[width=\textwidth]{Conceito_HIL_g.jpg}
		\fonte{Autor.}
	\end{minipage}
\end{figure}

As soluções comerciais analisadas são classificadas em duas: SW \textit{Application Programming Interface} (API) para o desenvolvimento da automatização; e HW para execução da simulação. Em todos os casos, a fabricante do HW visa o uso do seu produto com uma integração facilitada para um SW específico. Por mais que a integração de HW e SW de diferentes fabricantes seja realizável, esta pode ser dificultada. Alguns exemplos de soluções são apresentadas a seguir.

%\vspace{1eM}

A marca Speedgoat possui soluções de HW específica para simulações HIL, entre outras aplicações. Seu chassis ``\textit{Performance}'' (\fig{ChassisPerformance}), é uma ferramenta versátil de tempo real, que permite a execução da simulação em seu processador interno em alta velocidade. Esta simulação é programada e administrada pelo programa Matlab Simulink, que permite a interação virtual com o usuário, além de fornecer este chassis, a Speedgoat propõe \textit{racks} versáteis para a montagem da plataforma HIL.

\begin{figure}[ht]
	\caption{Chassis Performance}
    \label{fig:ChassisPerformance}
	\centering
	\begin{minipage}{.7\textwidth}
		\includegraphics[width=\textwidth]{Speedgoat.jpg}
		\fonte{\cite{Speedgoat}.}
	\end{minipage}
\end{figure}

Não diferente da sua concorrente, dSPACE fornece chassis de diferentes tamanhos e com especialidades diferentes para simulações HIL completas. Adicionalmente, a empresa fornece SW gestor de testes, SW gerador de modelos matemáticos, SW de automatização de teste, entre outros. A implementação em HW é simplificada com um \textit{rack} pré-montado para as necessidades do cliente, como mostra a \fig{ChassisSCALEXIO}.

\begin{figure}[ht]
	\caption{Chassis SCALEXIO}
    \label{fig:ChassisSCALEXIO}
	\centering
	\begin{minipage}{.65\textwidth}
		\includegraphics[width=\textwidth]{dSPACE.jpg}
		\fonte{\cite{dspace}.}
	\end{minipage}
\end{figure}

Vector Informatik é consagrada por ferramentas de análise de rede \textit{Controller Area Network} (CAN) em tempo real: CANoe e CANanalyser. A marca também possui um \textit{chassis} modular para a execução da plataforma HIL (\fig{ChassisVector}). O conceito proposto mistura funcionalidades previamente mencionadas com o sistema operacional de tempo real. Este SW possui a particularidade de ser voltado a veículos autopropelidos, apresentando assim, uma série de facilidades para a aplicação neste contexto.

\begin{figure}[ht]
	\caption{Chassis Vector}
    \label{fig:ChassisVector}
	\centering
	\begin{minipage}{.6\textwidth}
		\includegraphics[width=\textwidth]{Vector.jpg}
		\fonte{\cite{vector}.}
	\end{minipage}
\end{figure}

Um destaque entre as outras marcas, é a National Instruments, com renome no mercado. A empresa conta com o SW LabVIEW, que possui notoriedade de ser uma ferramenta largamente utilizada. Sobre outro aspecto, National Instruments conta com uma variedade de chassis, de classes diferentes, comumente para aplicações não específicas (exemplo \fig{ChassisNI}). Estes produtos não são projetados para plataformas HIL, porém bem utilizado com este propósito. Uma série de cartões para seus chassis e chassis de expansão estão disponíveis para as mais variadas medições de grandezas físicas. Entretanto, a marca não disponibiliza \textit{racks} pré-montados. Outrossim, estes chassis são projetados para trabalhar de forma otimizada com os próprios SW da empresa, LabVIEW, VeriStand e TestStand. Sendo o VeriStand um gestor de teste, com uma tela interativa para o usuário, e o TestStand, um automatizador de teste.

\begin{figure}[ht]
	\caption{Chassis NI PXIe-8880}
    \label{fig:ChassisNI}
	\centering
	\begin{minipage}{.7\textwidth}
		\includegraphics[width=\textwidth]{NI.jpg}
		\fonte{\cite{ni}.}
	\end{minipage}
\end{figure}

Atualmente, por conta de sua repercussão e qualidade, estas são as soluções mais divulgadas no mercado. No entanto, todos os casos de uso requerem que a metodologia de teste seja implementada. Sendo que esta varia com o produto a ser testado. Resumidamente, estas soluções buscam facilitar o desenvolvimento do sistema HIL, com base em suas funcionalidades comuns. Neste aspecto, este presente documento não se atentará a implementação do HW de instrumentação para HIL, porém, à \textbf{metodologia} de teste e à \textbf{arquitetura} do HW HIL em si. A metodologia de teste implementada será para máquinas agrícolas autopropelidas, com base na automatização de CT - o que não há registro em trabalhos anteriores.


























